const { Client } = require('tplink-smarthome-api');
const client = new Client();

client.getDevice({ host: '10.0.0.176' }).then((device) => {
	  // device.getSysInfo().then(console.log);

  device.lighting.setLightState({
	  on_off : true,
	  color_temp: 4000,
	  brightness: 50
  }).then(console.log);
});

